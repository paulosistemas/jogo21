package com.Itau;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Mesa mesa = new Mesa();
        Jogador jogador1 = new Jogador("Paulo");
        System.out.println("O jogo vai começar! " + jogador1.getNome());

        boolean continuar = true;
        while(continuar){
            int mao = jogador1.calcularMao(mesa.entregarCarta());
            if (mao <= 21) {
                System.out.println("Sua mão está com " + mao);
                System.out.println("mais uma carta (s/n)?");
                Scanner opcao = new Scanner(System.in);
                continuar = opcao.next().equals("s");
            }
            else{
                continuar = false;
            }
        }
//        jogador1.

    }
}
