package com.Itau;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

//        2
//        3
//        4
//        5
//        6
//        7
//        8
//        9
//        J Valete - 10
//        Q Dama- 10
//        K Rei - 10
//        A As - 1
//
//      0  copas
//      1  espada
//      2  ouro
//      3  paus

public class Mesa{
    private int[][] cartas = new int[4][12];

    public Mesa(){
        System.out.println(" inicializando a mesa...");
        for (int i=0; i<4;i++){
            for (int j=0; j<12; j++){
                cartas[i][j]=((j<10) ? j+1 : 10);
//                System.out.println("Carta carregada: " + cartas[i][j]);
            }
        }
    }

    public int entregarCarta(){
        int cartaEscolhida = 0;
        Random naipe = new Random();
        Random valorCarta = new Random();
        int naipeEscolhido = naipe.nextInt(3);
        int valorCartaEscolhido = valorCarta.nextInt(11);
        boolean cartaDentroBaralho = true;
        while(cartaDentroBaralho){
//            System.out.println("Naipe Escolhido:" + naipeEscolhido + "valor Carta: " + valorCartaEscolhido);
//            System.out.println("Carta Escolhida: " + cartas[naipeEscolhido][valorCartaEscolhido]);
            if ( cartas[naipeEscolhido][valorCartaEscolhido] != 0 ){
                cartaEscolhida = cartas[naipeEscolhido][valorCartaEscolhido];
                cartas[naipeEscolhido][valorCartaEscolhido] = 0;
                cartaDentroBaralho = false;
            }
            else{
                naipeEscolhido = naipe.nextInt(3);
                valorCartaEscolhido = valorCarta.nextInt(11);
            }
        }
        mostrarCarta(naipeEscolhido, valorCartaEscolhido);
        return cartaEscolhida;
    }

    public void mostrarCarta(int naipeEscolhido, int valorCartaEscolhida){
        String naipe;
        String valorCarta;
        valorCartaEscolhida++;

        switch(naipeEscolhido){
            case 0:
                naipe = "Copas";
                break;
            case 1:
                naipe = "Espada";
                break;
            case 2:
                naipe = "Ouro";
                break;
            case 3:
                naipe = "Paus";
                break;
            default:
                naipe = "";
        }

        switch(valorCartaEscolhida){
            case 10:
                valorCarta = "Valete";
                break;
            case 11:
                valorCarta = "Dama";
                break;
            case 12:
                valorCarta = "Rei";
                break;
            case 1:
                valorCarta = "As";
                break;
            default:
                valorCarta =  Integer.toString(valorCartaEscolhida);
                break;
        }
        System.out.println("Carta retirada: " + valorCarta + " de " + naipe);
    }
}
