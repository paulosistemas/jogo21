package com.Itau;

public class Jogador {
    private int maoCartas;
    private String nome;
    private int melhorResultado;

    public int getMaoCartas() {
        return maoCartas;
    }

    public String getNome() {
        return nome;
    }

    public int getMelhorResultado() {
        return melhorResultado;
    }

    public Jogador(String nome){
        this.nome = nome;
        maoCartas = 0;
    }

    public int calcularMao(int novaCarta){
        maoCartas += novaCarta;
        if (maoCartas > 21){
            System.out.println("Ahhh, perdeu!" + maoCartas);
        }
        return maoCartas;
    }

    public int calcularMelhorResultado(int resultado){
        if (resultado <= 21 && resultado > melhorResultado){
            melhorResultado = resultado;
        }
        return melhorResultado;
    }
}
